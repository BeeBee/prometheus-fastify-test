import { build } from "../src/app";
import { Beer } from "../src/entities/beer.entity";
import { createDummyEntityFactories } from "./utils/entity.factory";
import { db } from "../src/config/db";
import { FastifyInstance } from "fastify";
import { CreateBeerInput } from "../src/graphql/inputs/createBeer.input";

describe("Test /beer endpoints", () => {
    let app: FastifyInstance;

    beforeAll(async () => {
        const databaseConfig = Object.assign(db, {
            database: "test_db",
            synchronize: true,
            dropSchema: true,
        });

        app = await build({
            db: databaseConfig,
        });
    });

    test("GET /beer returns multiple records", async () => {
        // Arrange
        const {
            createBeer,
        } = createDummyEntityFactories(app.orm);

        const beer = await createBeer();
        await createBeer({
            brewery: beer.brewery,
        });


        // Act
        const result = await app.inject({
            method: "GET",
            url: "/beer"
        });

        // Assert
        expect(result.statusCode).toEqual(200);
        const responseBody = JSON.parse(result.body) as Beer[];
        expect(responseBody.length).toEqual(2);
        for (const responseBeer of responseBody) {
            expect(responseBeer.brewery).toEqual(beer.brewery);
        }
    });


    test("POST /beer create a new beer", async () => {
        // Arrange
        const {
            createBrewery,
        } = createDummyEntityFactories(app.orm);
        const brewery = await createBrewery();
        const newBeer = new CreateBeerInput();
        newBeer.name = "Nové pivo";
        newBeer.breweryId = brewery.id!;

        // Act
        const result = await app.inject({
            method: "POST",
            url: "/beer",
            payload: newBeer,
        });

        // Assert
        expect(result.statusCode).toEqual(200);
        const responseBody = JSON.parse(result.body) as Beer;
        const beer = await app.orm.getRepository<Beer>(Beer).findOne({ where: { id: responseBody.id }, relations: ["brewery"] });
        expect(responseBody).toEqual(beer);
    });

    afterAll(async () => {
        await app.close();
    });
});

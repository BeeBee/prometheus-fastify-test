import { Beer } from "../../src/entities/beer.entity";
import { Brewery } from "../../src/entities/brewery.entity";
import { DataSource } from "typeorm";
import { generateUniqueId } from "./helpers";

interface DummyEntityFactory<TEntity> {
    (properties?: Partial<TEntity>): Promise<TEntity>;
}
export interface DummyEntityFactories {
    createBeer: DummyEntityFactory<Beer>;
    createBrewery: DummyEntityFactory<Brewery>;
}

export const createDummyEntityFactories = (
    orm: DataSource,
): DummyEntityFactories => {
    const createBeer = async (properties: Partial<Beer> = {}): Promise<Beer> => {
        const id = generateUniqueId();
        const name = "beer" + id;

        const brewery = await createBrewery();

        const data: Beer = {
            name,
            brewery,
            ...properties,
        };

        return await orm.getRepository<Beer>(Beer).save(data);
    };

    const createBrewery = async (properties: Partial<Brewery> = {}): Promise<Brewery> => {
        const id = generateUniqueId();
        const name = "brewery" + id;

        const data: Brewery = {
            name,
            ...properties,
        };

        return await orm.getRepository<Brewery>(Brewery).save(data);
    };

    return {
        createBeer,
        createBrewery,
    };
};

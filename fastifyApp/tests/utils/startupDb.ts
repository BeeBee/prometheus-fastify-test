import { Client } from "pg";
import process from "process";

async function createDatabase() {
    const dbName = "test_db";
    const client = new Client({
        port: ~~(process.env.DB_PORT!) ?? 5432,
        host: process.env.DB_HOST ?? "db_host",
        user: process.env.DB_USER ?? "user",
        password: process.env.DB_PASSWORD ?? "db_password",
        database: process.env.DB_NAME ?? "db_database",
    });

    try {
        await client.connect();

        // Check if the database already exists
        const res = await client.query(`SELECT 1 FROM pg_database WHERE datname='${dbName}'`);
        if (res.rows.length === 0) {
            // Create the database if it doesn't exist
            await client.query(`CREATE DATABASE "${dbName}"`);
            // console.log(`Database ${dbName} created successfully.`);
        } else {
            // console.log(`Database ${dbName} already exists.`);
        }
    } catch (error) {
        console.error(`Error creating database: ${error}`);
    } finally {
        await client.end();
    }
}


global.beforeAll(async () => {
    await createDatabase();
});

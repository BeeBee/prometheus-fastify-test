let lastId = 0;

export const generateUniqueId = (): number => lastId += 1;

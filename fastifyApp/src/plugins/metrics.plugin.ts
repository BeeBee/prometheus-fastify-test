import {
    FastifyInstance,
    FastifyPluginAsync
} from "fastify";
import { DataSourceOptions } from "typeorm/data-source/DataSourceOptions";
import { fastifyPlugin } from "fastify-plugin";
import metrics from "fastify-metrics";

const metricsSetup: FastifyPluginAsync<DataSourceOptions> = async (fastify: FastifyInstance): Promise<void> => {
    fastify.register(metrics, {
        endpoint: "/metrics",
    });
};

export const metricPlugin = fastifyPlugin(metricsSetup);

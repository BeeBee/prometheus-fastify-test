import {
    FastifyInstance,
    FastifyPluginAsync
} from "fastify";
import { DataSourceOptions } from "typeorm/data-source/DataSourceOptions";
import { fastifyPlugin } from "fastify-plugin";
import swagger from "@fastify/swagger";
import swaggerUi from "@fastify/swagger-ui";

const swaggerSetup: FastifyPluginAsync<DataSourceOptions> = async (fastify: FastifyInstance): Promise<void> => {
    fastify.register(swagger, {
        openapi: {
            info: {
                title: "Beer API",
                description: "CRUD REST/GraphQL API",
                version: "0.1.0"
            },
            externalDocs: {
                url: "https://swagger.io",
                description: "Find more info here"
            },
        }
    });
    fastify.register(swaggerUi, {
        routePrefix: "/swagger"
    });
};

export const swaggerPlugin = fastifyPlugin(swaggerSetup);

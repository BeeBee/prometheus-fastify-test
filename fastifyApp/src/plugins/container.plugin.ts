import {
    FastifyInstance,
    FastifyPluginAsync
} from "fastify";
import { Beer } from "../entities/beer.entity";
import { fastifyPlugin } from "fastify-plugin";
import Container from "typedi";
import { Brewery } from "../entities/brewery.entity";
import {
    IBeerRepository,
    IBreweryRepository
} from "../di/tokens.di";

declare module "fastify" {
    interface FastifyInstance {
        container: typeof Container,
    }
}

const container: FastifyPluginAsync = async (fastify: FastifyInstance): Promise<void> => {
    Container.set(IBeerRepository, fastify.orm.getRepository<Beer>(Beer));
    Container.set(IBreweryRepository, fastify.orm.getRepository<Brewery>(Brewery));

    fastify.decorate("container", Container);
};

export const containerPlugin = fastifyPlugin(container);

import {
    FastifyInstance,
    FastifyPluginAsync
} from "fastify";
import { fastifyPlugin } from "fastify-plugin";
import { buildSchema } from "type-graphql";
import { BeerResolver } from "../graphql/resolvers/beer.resolver";
import {
    ApolloServer,
    BaseContext
} from "@apollo/server";
import fastifyApollo, { fastifyApolloDrainPlugin } from "@as-integrations/fastify";
import { HelloResolver } from "../graphql/resolvers/hello.resolver";

const apolloInit: FastifyPluginAsync = async (fastify: FastifyInstance): Promise<void> => {
    const schema = await buildSchema({
        resolvers: [
            BeerResolver,
            HelloResolver,
        ],
        container: fastify.container,
    });

    const apollo = new ApolloServer<BaseContext>({
        schema,
        plugins: [
            fastifyApolloDrainPlugin(fastify),
        ],
    });
    await apollo.start();

    fastify.register(fastifyApollo(apollo));
};

export const apolloPlugin = fastifyPlugin(apolloInit);

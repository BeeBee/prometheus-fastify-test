import {
    FastifyInstance,
    FastifyPluginAsync
} from "fastify";
import { DataSource } from "typeorm";
import { Beer } from "../entities/beer.entity";
import { Brewery } from "../entities/brewery.entity";
import { DataSourceOptions } from "typeorm/data-source/DataSourceOptions";
import { fastifyPlugin } from "fastify-plugin";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";

declare module "fastify" {
    interface FastifyInstance {
        orm: DataSource,
    }
}

const dbConnector: FastifyPluginAsync<DataSourceOptions> = async (fastify: FastifyInstance, opts: DataSourceOptions): Promise<void> => {
    const orm = new DataSource({
        logging: false,
        entities: [Beer, Brewery],
        namingStrategy: new SnakeNamingStrategy(),
        ...opts,
    });
    await orm.initialize();

    fastify.decorate("orm", orm);
};

export const dbPlugin = fastifyPlugin(dbConnector);

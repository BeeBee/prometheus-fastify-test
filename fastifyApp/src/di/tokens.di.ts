import { Repository } from "typeorm";
import { Beer } from "../entities/beer.entity";
import { Token } from "typedi";
import { Brewery } from "../entities/brewery.entity";

export interface IBeerRepository extends Repository<Beer> {}

export const IBeerRepository = new Token<Repository<Beer>>();
export const IBreweryRepository = new Token<Repository<Brewery>>();

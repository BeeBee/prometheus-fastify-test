import {
    fastify,
    FastifyInstance,
} from "fastify";

import { hiRoute } from "./routes/hi.route";
import { dbPlugin } from "./plugins/db.plugin";
import { beerRoute } from "./routes/beer/beer.route";
import { containerPlugin } from "./plugins/container.plugin";
import { apolloPlugin } from "./plugins/apolloPlugin";
import { AppOptions } from "./config/config";
import { swaggerPlugin } from "./plugins/swagger.plugin";
import { metricPlugin } from "./plugins/metrics.plugin";

export const build = async (
    opts?: AppOptions
): Promise<FastifyInstance> => {
    const app = fastify(opts?.app);

    if (opts?.db) {
        app.register(dbPlugin, opts.db);
    } else {
        throw new Error("Missing db options");
    }
    await app.register(containerPlugin);
    app.register(swaggerPlugin);
    app.register(metricPlugin);

    app.register(apolloPlugin);

    app.register(hiRoute);
    app.register(beerRoute);

    return app;
};

import {
    Arg,
    Int,
    Mutation,
    Query,
    Resolver
} from "type-graphql";
import { Beer } from "../../entities/beer.entity";
import { BeerService } from "../../services/beer.service";
import { CreateBeerInput } from "../inputs/createBeer.input";
import { UpdateBeerInput } from "../inputs/updateBeer.input";
import { Service } from "typedi";

@Service()
@Resolver()
export class BeerResolver {
    public constructor(
        private readonly beerService: BeerService,
    ) {}

    @Query(() => [Beer])
    async beers(): Promise<Beer[]> {
        return this.beerService.getAll();
    }

    @Mutation(() => Beer)
    async addBeer(
        @Arg("CreateBeerInput") createBeerInput: CreateBeerInput,
    ): Promise<Beer> {
        return this.beerService.create(createBeerInput);
    }

    @Mutation(() => Beer)
    async updateBeer(
        @Arg("UpdateBeerInput") updateBeerInput: UpdateBeerInput,
    ): Promise<Beer> {
        return await this.beerService.update(updateBeerInput);
    }

    @Mutation(() => Boolean)
    async deleteBeer(@Arg("id", () => Int) id: number): Promise<boolean> {
        await this.beerService.delete(id);

        return true;
    }
}

import {
    Field,
    InputType,
    Int
} from "type-graphql";

@InputType()
export class UpdateBeerInput {
    @Field(() => Int)
    id!: number;

    @Field()
    name!: string;

    @Field(() => Int)
    breweryId!: number;
}

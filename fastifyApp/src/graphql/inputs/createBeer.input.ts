import {
    Field,
    InputType,
    Int
} from "type-graphql";

@InputType()
export class CreateBeerInput {
    @Field()
    name!: string;

    @Field(() => Int)
    breweryId!: number;
}

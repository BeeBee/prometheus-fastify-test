import { build } from "./app";
import { config } from "./config/config";

/**
 * Run the server!
 */
const start = async (): Promise<void> => {
    const server = await build(config);

    try {
        await server.listen({ port: 5000, host: "0.0.0.0" });
    } catch (err) {
        server.log.error(err);
        process.exit(1);
    }
};

start();

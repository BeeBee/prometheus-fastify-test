import { DataSourceOptions } from "typeorm";
import { db } from "./db";
import { FastifyServerOptions } from "fastify";
import {
    Server,
    ServerOptions
} from "http";
import { FastifyBaseLogger } from "fastify/types/logger";

export interface AppOptions {
    app?: FastifyServerOptions<Server, FastifyBaseLogger> & { http?: ServerOptions | null },
    db?: DataSourceOptions,
}
export const config: AppOptions = {
    app: {
        logger: true,
    },
    db: db,
};

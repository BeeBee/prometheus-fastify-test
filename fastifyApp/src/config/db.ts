import * as process from "process";
import { DataSourceOptions } from "typeorm/data-source/DataSourceOptions";

export const db: DataSourceOptions = {
    type: "postgres",
    port: ~~(process.env.DB_PORT!) ?? 5432,
    host: process.env.DB_HOST ?? "db_host",
    username: process.env.DB_USER ?? "user",
    password: process.env.DB_PASSWORD ?? "db_password",
    database: process.env.DB_NAME ?? "db_database",
};

import {
    Inject,
    Service
} from "typedi";
import { Repository } from "typeorm";
import { Beer } from "../entities/beer.entity";
import { CreateBeerInput } from "../graphql/inputs/createBeer.input";
import { Brewery } from "../entities/brewery.entity";
import { UpdateBeerInput } from "../graphql/inputs/updateBeer.input";
import {
    IBeerRepository,
    IBreweryRepository
} from "../di/tokens.di";

@Service()
export class BeerService {
    public constructor(
        @Inject(IBeerRepository)
        private readonly beerRepository: Repository<Beer>,
        @Inject(IBreweryRepository)
        private readonly breweryRepository: Repository<Brewery>,
    ) {}

    public async getAll(): Promise<Beer[]> {
        return this.beerRepository.find({ relations: { brewery: true } });
    }

    public async getOne(id: number): Promise<Beer> {
        const beer = await this.beerRepository.findOne({
            where: { id },
            relations: ["brewery"],
        });

        if (!beer) {
            throw new Error(`The beer with id: ${id} does not exist!`);
        }
        return beer;
    }

    public async create(createBeerInput: CreateBeerInput): Promise<Beer> {
        const brewery = await this.breweryRepository.findOne({ where: { id: createBeerInput.breweryId } });

        if (!brewery) {
            throw new Error(`The brewery with id: ${createBeerInput.breweryId} does not exist!`);
        }

        // TODO: create factory method
        const beer = new Beer();
        const newBeerValues: Partial<Beer> = {
            name: createBeerInput.name,
            brewery: brewery,
        };
        Object.assign(beer, newBeerValues);

        return this.beerRepository.save(beer);
    }

    public async update(updateBeerInput: UpdateBeerInput): Promise<Beer> {
        const beer = await this.beerRepository.findOne({ where: { id: updateBeerInput.id }, relations: ["brewery"] });

        if (!beer) {
            throw new Error(`The beer with id: ${updateBeerInput.id} does not exist!`);
        }

        const newBeerValues: Partial<Beer> = {};

        if (updateBeerInput.name) {
            newBeerValues.name = updateBeerInput.name;
        }

        if (updateBeerInput.breweryId) {
            const brewery = await this.breweryRepository.findOne({ where: { id: updateBeerInput.breweryId } });

            if (!brewery) {
                throw new Error(`The brewery with id: ${updateBeerInput.breweryId} does not exist!`);
            }

            newBeerValues.brewery = brewery;
        }
        Object.assign(beer, newBeerValues);

        return this.beerRepository.save(beer);
    }

    public async delete(id: number): Promise<void> {
        const beer = await this.beerRepository.findOne({
            where: { id },
        });

        if (!beer) {
            throw new Error(`The beer with id: ${id} does not exist!`);
        }

        await this.beerRepository.delete(beer);
    }
}

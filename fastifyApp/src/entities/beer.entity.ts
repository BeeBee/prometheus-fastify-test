import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn
} from "typeorm";
import { Brewery } from "./brewery.entity";
import {
    Field,
    ID,
    ObjectType
} from "type-graphql";

@Entity()
@ObjectType()
export class Beer {
    @PrimaryGeneratedColumn()
    @Field(() => ID)
    id?: number;

    @Column()
    @Field()
    name!: string;

    @ManyToOne(() => Brewery, { cascade: true })
    @JoinColumn()
    @Field(() => Brewery)
    brewery!: Brewery;
}

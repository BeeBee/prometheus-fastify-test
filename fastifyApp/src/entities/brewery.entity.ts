import {
    Field,
    ID,
    ObjectType
} from "type-graphql";
import {
    Column,
    Entity,
    PrimaryGeneratedColumn
} from "typeorm";

@Entity()
@ObjectType()
export class Brewery {
    @PrimaryGeneratedColumn()
    @Field(() => ID)
    id?: number;

    @Column()
    @Field()
    name!: string;
}

import { FastifyInstance } from "fastify";
import { BeerService } from "../../services/beer.service";
import { ParamId } from "./requests/paramId.interface";

export const deleteBeerRoute = async (fastify: FastifyInstance): Promise<void> => {
    const bs = fastify.container.get<BeerService>(BeerService);

    fastify.delete<{
        Params: ParamId,
    }>(
        "/beer/:id",
        {
            schema: {
                params: {
                    id: { type: "number" },
                }
            }
        },
        async function (request): Promise<void> {
            const { id } = request.params;

            await bs.delete(id);
        },
    );
};

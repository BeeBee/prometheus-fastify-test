import { FastifyInstance } from "fastify";
import { BeerService } from "../../services/beer.service";
import { ParamId } from "./requests/paramId.interface";
import { Beer } from "../../entities/beer.entity";

export const getBeerRoute = async (fastify: FastifyInstance): Promise<void> => {
    const bs = fastify.container.get<BeerService>(BeerService);

    fastify.get<{
        Params: ParamId,
        Reply: Beer,
    }>(
        "/beer/:id",
        {
            schema: {
                params: {
                    id: { type: "number" },
                }
            }
        },
        async function (request): Promise<Beer> {
            const { id } = request.params;
            return bs.getOne(id);
        },
    );
};

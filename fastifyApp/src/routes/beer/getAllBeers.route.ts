import { FastifyInstance } from "fastify";
import { BeerService } from "../../services/beer.service";
import { Beer } from "../../entities/beer.entity";

export const getAllBeersRoute = async (fastify: FastifyInstance): Promise<void> => {
    const bs = fastify.container.get<BeerService>(BeerService);

    fastify.get("/beer", async (): Promise<Beer[]> => bs.getAll());
};

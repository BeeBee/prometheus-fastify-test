import { FastifyInstance } from "fastify";
import { BeerService } from "../../services/beer.service";
import { ParamId } from "./requests/paramId.interface";
import { UpdateBeerPayload } from "./requests/updateBeerRequest.payload";
import { Beer } from "../../entities/beer.entity";

export const updateBeerRoute = async (fastify: FastifyInstance): Promise<void> => {
    const bs = fastify.container.get<BeerService>(BeerService);

    fastify.post<{
        Params: ParamId,
        Body: UpdateBeerPayload,
        Reply: Beer,
    }>(
        "/beer/:id",
        {
            schema: {
                params: {
                    id: { type: "number" },
                },
                body: {
                    name: { type: "string" },
                    breweryId: { type: "number" },
                }
            }
        },
        async function (request) {
            const { id } = request.params;
            const { name, breweryId} = request.body;

            return bs.update({
                id,
                name,
                breweryId,
            });
        },
    );
};

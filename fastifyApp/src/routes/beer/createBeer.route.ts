import { FastifyInstance } from "fastify";
import { BeerService } from "../../services/beer.service";
import { Beer } from "../../entities/beer.entity";
import { CreateBeerInput } from "../../graphql/inputs/createBeer.input";

export const createBeerRoute = async (fastify: FastifyInstance): Promise<void> => {
    const bs = fastify.container.get<BeerService>(BeerService);

    fastify.post<{
        Body: CreateBeerInput,
        Reply: Beer,
    }>(
        "/beer",
        {
            schema: {
                params: {
                    id: { type: "number" },
                },
                body: {
                    name: { type: "string" },
                    breweryId: { type: "number" },
                }
            }
        },
        async function (request) {
            const { name, breweryId} = request.body;

            return bs.create({
                name,
                breweryId,
            });
        },
    );
};

import { FastifyInstance } from "fastify";
import { getAllBeersRoute } from "./getAllBeers.route";
import { getBeerRoute } from "./getBeer.route";
import { updateBeerRoute } from "./updateBeer.route";
import { createBeerRoute } from "./createBeer.route";
import { deleteBeerRoute } from "./deleteBeer.route";

export const beerRoute = async (fastify: FastifyInstance): Promise<void> => {
    fastify.register(getAllBeersRoute);
    fastify.register(getBeerRoute);
    fastify.register(updateBeerRoute);
    fastify.register(createBeerRoute);
    fastify.register(deleteBeerRoute);
};

export interface UpdateBeerPayload {
    name: string,
    breweryId: number,
}

import { FastifyInstance } from "fastify";

export interface HiResponse {
    hello: string;
}
export const hiRoute = async (fastify: FastifyInstance): Promise<void> => {
    fastify.get("/hi", async (): Promise<HiResponse> => ({ hello: "world" }));
};

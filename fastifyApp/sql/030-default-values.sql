INSERT INTO "brewery" ("name") VALUES
	('piiivovar'),
	('chmelenice');

INSERT INTO "beer" ("name", "brewery_id") VALUES
	('lahodný mok',	1),
	('vostena',	1),
	('zatecka mana',	2);

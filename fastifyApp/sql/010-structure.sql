-- Adminer 4.8.1 PostgreSQL 16.1 dump

DROP TABLE IF EXISTS "beer";
DROP SEQUENCE IF EXISTS beer_id_seq;
CREATE SEQUENCE beer_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647;

CREATE TABLE "beer" (
	"id" integer DEFAULT nextval('beer_id_seq') NOT NULL,
	"name" character varying(100) NOT NULL,
	"brewery_id" integer NOT NULL,
	CONSTRAINT "beer_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

DROP TABLE IF EXISTS "brewery";
DROP SEQUENCE IF EXISTS brewery_id_seq;
CREATE SEQUENCE brewery_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647;

CREATE TABLE "brewery" (
	"id" integer DEFAULT nextval('brewery_id_seq') NOT NULL,
	"name" character varying(50) NOT NULL,
	CONSTRAINT "brewery_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

ALTER TABLE ONLY "beer" ADD CONSTRAINT "beer_brewery_id_fkey" FOREIGN KEY (brewery_id) REFERENCES brewery(id) ON DELETE CASCADE NOT DEFERRABLE;

-- 2024-01-26 23:59:14.845416+01

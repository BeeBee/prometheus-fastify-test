-include local/Makefile

# Load environment variables from .env file
include .env
export $(shell sed 's/=.*//' .env)

## Global environment variables
DOCKER_COMPOSE_BIN ?= "docker-compose"

## Local environment variables
ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
UID := $(shell id -u)
GID := $(shell id -g)
DOCKER_COMPOSE := $(DOCKER_COMPOSE_BIN)
RSHELL := $(DOCKER_COMPOSE) exec -T --user=$(UID):$(GID) app
RSHELL_TTY := $(DOCKER_COMPOSE) exec --user=$(UID):$(GID) app
RSHELL_RUN_TTY := $(DOCKER_COMPOSE) run  --rm --user=$(UID):$(GID) app
RSHELL_RUN_WITH_PORTS_TTY := $(DOCKER_COMPOSE) run  --rm --user=$(UID):$(GID) --service-ports app

export RUNTIME_IMAGE := fastify-runtime
export YARN_CACHE_FOLDER := /app/.yarn-cache

.NOPARALLEL .PHONY .SILENT: all
all:
	@$(DOCKER_COMPOSE) pull --ignore-pull-failures
	$(MAKE) run
	$(MAKE) install

.PHONY .SILENT: run
run: ## Run development environment
	APP_USER="${UID}:${GID}" $(DOCKER_COMPOSE) up -d

.PHONY .SILENT: stop
stop: ## Stop
	@$(DOCKER_COMPOSE) stop

.PHONY .SILENT: cleanup
cleanup: ## Stop and remove all containers
	@$(DOCKER_COMPOSE) down

.PHONY .SILENT: build-image
build-image: ## Rebuild runtime image
	@$(DOCKER_COMPOSE) pull --ignore-pull-failures
	@$(DOCKER_COMPOSE) up -d --build

.PHONY .SILENT: install
install:  ## Install all dependencides
	$(RSHELL_RUN_TTY) sh -c "yarn install --network-timeout 1000000000"

.PHONY: fmt-check
lint-check: ## Run eslint
	$(RSHELL_RUN_TTY) sh -c "yarn run eslint:check"

.PHONY: lint-fix
lint-fix: ## Run and fix lint errors
	$(RSHELL_RUN_TTY) sh -c "yarn run eslint:fix"

.PHONY .SILENT: exec
exec: ## Run Bash shell inside dev server
	$(RSHELL_RUN_TTY) bash

.PHONY .SILENT: help
help: ## Print help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY .SILENT: test-unit
test-unit: ## Runs unit tests
	$(RSHELL_RUN_TTY) sh -c "NODE_ENV=testing yarn run test"

.PHONY .SILENT: test-e2e
test-integration: ## Runs e2e tests
	$(RSHELL_RUN_TTY) sh -c "NODE_ENV=testing yarn run test:integration --detectOpenHandles"

.PHONY .SILENT: test
test: ## Runs all tests
	$(MAKE) test-unit
	$(MAKE) test-integration

.PHONY .SILENT: test-by-path
test-by-path: ## Test by file path | make test-by-path FP=./test/submitCataloguePriceDraftsFacade.e2e-spec.ts
	$(RSHELL_RUN_TTY) sh -c "yarn jest --detectOpenHandles -i --config ./test/jest-general.json "$(FP)""

.PHONY : test-coverage
test-coverage: ## Run tests with coverage
	$(RSHELL_RUN_TTY) sh -c "yarn run test:coverage --detectOpenHandles --forceExit --config ./test/jest-general.json"

.PHONY .SILENT: init-database
init-database: ## Inits database for dev and test purposes
	@echo 'DROP DATABASE IF EXISTS `supply-order-service`; CREATE DATABASE `supply-order-service`' | $(DOCKER_COMPOSE) exec -T percona mysql -u root -proot && \
	$(DOCKER_COMPOSE) exec -T percona mysql -u root -proot supply-order-service < sql/structure.sql

.PHONY: db-reset
db-reset: ## Reset DB data to default stater
	bash ./bin/reset-db.sh

.PHONY: run-debug
run-debug: ## Run debug session
	$(DOCKER_COMPOSE) stop app
	$(RSHELL_RUN_WITH_PORTS_TTY) sh -c "yarn start:debug"

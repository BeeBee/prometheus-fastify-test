#!/bin/bash

SCRIPT_NAME="reset-db.sh"

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m'

# Test path where script is ran from
# Set right work path
# TODO Find better way to get actual path
if [[ -f "$(pwd)/bin/${SCRIPT_NAME}" ]];  then
  WORK_DIR=$(pwd)
elif [[ -f "$(pwd)/${SCRIPT_NAME}" ]]; then
  WORK_DIR=$(dirname $(pwd))
else
  echo -e "${RED}Run script from project root or root/bin folder.${NC}"
  return 1
fi

# Import
echo -e "${YELLOW}DROPPING AND CREATING DATABASE${NC}"
#echo 'DROP DATABASE IF EXISTS `supply_order_service`; CREATE DATABASE `supply_order_service`;' | docker-compose exec -T percona mysql -uroot -proot

cat "${WORK_DIR}"/sql/*.sql >> "${WORK_DIR}"/sql/schema.sql
docker-compose exec -T postgres-db psql -U ${DB_USER} -W ${DB_PASSWORD} -d ${DB_DATABASE} < "${WORK_DIR}"/sql/schema.sql

# Cleanup
echo -e "${YELLOW}REMOVING TEMPORARY SQL FILE${NC}"
rm "${WORK_DIR}"/sql/schema.sql

echo -e "${GREEN}DONE${NC}"
